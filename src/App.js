import React from 'react';
import logo from './logo.svg';
import './App.css';
import UsersList from './components/users';
import Router from './routes';


function App() {
  return (
    <div>
      <Router/>
    </div>
  );
}

export default App;
