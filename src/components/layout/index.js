import React from 'react';
import {  Layout as AntdLayout, Menu } from 'antd'
import { Link } from 'react-router-dom';


function Layout({children, title = ''}){
    return <AntdLayout>
        <AntdLayout.Sider
            breakpoint="lg"
            collapsedWidth="0"
        >
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>

                 <Link to="/">
                    <Menu.Item key="1" icon={<div/>}>
                        Home
                    </Menu.Item>
                </Link>
                <Link to="/users">
                    <Menu.Item key="2" icon={<div/>}>
                        Users list
                    </Menu.Item>
                </Link>
            </Menu>
        </AntdLayout.Sider>
        <AntdLayout>
            <AntdLayout.Header style={{background: "#fff"}}>
                {title}
            </AntdLayout.Header>
            <AntdLayout.Content>
                <div style={{padding: 20}}>
                    {children}
                </div>
            </AntdLayout.Content>
        </AntdLayout>
    </AntdLayout>
}

export default Layout