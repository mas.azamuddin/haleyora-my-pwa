import React from 'react';
import Axios from 'axios'
import { Table, Pagination } from 'antd'
import 'antd/dist/antd.css'; 
import Layout from '../layout';

const fetchUsers = async (limit = 10, skip = 0) => {

    let { data: {users, count} } = await Axios.get(`${process.env.REACT_APP_API_HOST}/users?limit=${limit}&skip=${skip}`);

    return {users, count};
}

const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'City',
      dataIndex: 'city',
      key: 'city',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
];


function UsersList() {

    const [users, setUsers] = React.useState([]);
    const [skip, setSkip] = React.useState(0);
    const [count, setCount] = React.useState(0);

    const LIMIT = 5;

    React.useEffect(() => {
        fetchUsers(LIMIT, skip)
        .then(users => {
            setUsers(users.users)
            setCount(users.count)
        })
    }, []);

    const handlePaginationChange = async (data) => {
        let {users} = await fetchUsers(LIMIT, (data * LIMIT) - LIMIT)
        setUsers(users);
    }

    return (
    <Layout title="Daftar User">
      <div style={{height: '100vh'}}>

        <Table
          columns={columns}
          dataSource={users}
          pagination={{position: []}}
        />

        <br/>

        <Pagination
            defaultCurrent={1}
            pageSize={LIMIT}
            total={count}
            onChange={handlePaginationChange}
        />

      </div>
      </Layout>
    );
  }
  
  export default UsersList;