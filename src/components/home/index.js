import React from 'react'; 
import { Link } from 'react-router-dom'
import Layout from '../layout';


const HomePage = () => {
    return <Layout title="Welcome to App">
        <div style={{height: '100vh'}}>
            <Link to={"/users"}> Users list</Link>
        </div>
    </Layout>
}

export default HomePage