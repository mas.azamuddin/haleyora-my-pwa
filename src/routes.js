import { HashRouter, Route } from 'react-router-dom'
import HomePage from './components/home';
import Layout from './components/layout';
import UsersList from './components/users'


function Router(){
    return <>
        <HashRouter>
           <Route path="/" exact component={HomePage}/>
           <Route path="/users" exact component={UsersList} />
        </HashRouter>
    </>
}

export default Router;